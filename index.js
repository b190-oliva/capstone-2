const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
require('dotenv/config');

mongoose.connect("mongodb+srv://39dexshaman:Chowking123@wdc028-course-booking.8f4g3cd.mongodb.net/db-ecommerce?retryWrites=true&w=majority",
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.listen(process.env.PORT || 4000, () => {console.log(`API now online at port ${process.env.PORT || 4000}`)});