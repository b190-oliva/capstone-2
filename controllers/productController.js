const Product = require("../models/products.js");
const bodyParser = require("body-parser"),
      fs = require("fs"),
	  multer = require('multer');
  
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
  
const upload = multer({ storage: storage });

//add products
module.exports.addProduct= (reqbody) => {
	let newProduct = new Product({
	category: reqbody.category,
	productType: reqbody.productType,
	productName: reqbody.name,
	description: reqbody.description,
	price: reqbody.price,
	img: {
            data: fs.readFileSync(path.join(__dirname + '/uploads/' + req.file.filename)),
            contentType: 'image/png'
        }
	})
	return newProduct.save(reqbody).then((result,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};

// archive a product (admin only)

module.exports.archive = (req) => {
	console.log(req.body.isActive);
	console.log(req.params.productId);
	let update = {
		isActive: req.body.isActive
	}
	return Product.findByIdAndUpdate(req.params.productId, update).then((result,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};

// re-activating of a product

module.exports.activateProduct = (params) => {
	let update = {
		isActive: true
	}
	return Product.findByIdAndUpdate(params.productId,update).then((result,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};

// preview a single product

module.exports.previewProduct = (params) => {
	return Product.findById(params.productId).then((result,error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}

// preview of all 'active' products

module.exports.allProducts = () => {
	return Product.find({}).then(result => {return result});
};

// updating a specific product

module.exports.update = (reqbody,params) => {
		let update = {
		"productName": reqbody.productName,
		"description": reqbody.description,
		"price": reqbody.price,
		"onSale": reqbody.onSale
		}
		return Product.findByIdAndUpdate(params.productId, update).then((result,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		});
};
