const User = require("../models/users.js");
const Product = require("../models/products.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqbody) => {
	return User.find({email: reqbody.email}).then(result => {
		if(result.length <= 0 || result.length == null){
			let newUser = new User({
				firstname: reqbody.firstname,
				lastname: reqbody.lastname,
				email: reqbody.email,
				mobilenumber: reqbody.mobilenumber,
				password: bcrypt.hashSync(reqbody.password, 10)
			})
			return newUser.save(reqbody).then((result,error) =>{
				if(error){
					return error;
				}
				else{
					return true;
				};
			});
		}
		else{
			return false;
		}
	})
};

// function for login

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result === null || result === undefined){
			return false;
		}
		else {
			const isPwCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPwCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else
			{
				return false;
			}
		};
	});
};


module.exports.getProfile = (data) => { 
	return User.findById(data.userId).then(result => { 
		result.password = ""; 
		return result; 
	}); 
};

module.exports.updateUser = (params) => {
	let update = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(params.userId, update).then((result,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};
module.exports.checkOut = (data) => {
	let update = {
		productId: data.productId
	}
   return User.findByIdAndUpdate({_id:data.userId},{ $push: {orders: update}}).then((result,error)=>{
   		if(error){
			return false;
		}
		else{
			return true;
		}
   })
};		

module.exports.getAllOrders = () => {
	return User.find({},{orders: 1});
};

module.exports.getMyOrders = (data) => {
	return User.findById(data.id, {orders: 1});
};

module.exports.removeOrder = async (data) => {
	let update = {
		orders: []
	}
	return User.findByIdAndUpdate(data,update).then((result,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};