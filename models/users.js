const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstname: {
		type: String,
		required: [true,"First name is required"]
	},
	lastname: {
		type: String,
		required: [true,"Last name is required"]
	},
	email: {
		type: String,
		required: [true,"Email is required"]
	},
	mobilenumber: {
		type: String,
		required: [true,"Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	password: {
		type: String,
		required: [true,"Password is required"]
	},
	orders: [{
		productId: {
			type: String,
			required: [true, "Product id is required"]
		},
		addedOn:{
			type: Date,
			default: new Date()
		},
		status:{
			type: String,
			default: "Available"
		}
	}]
});

module.exports = mongoose.model("users", userSchema);