const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	category:{
		type: String,
		required: [true, "Category is required"]
	},
	productType:{
		type: String,
		required: [true, "Product type is required"]
	},
	productName: {
		type: String,
		required: [true,"Product name is required"]
	},
	description: {
		type: String,
		required: [true,"Description is required"]
	},
	price: {
		type: Number,
		required: [true,"Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	onSale: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
    image: {
        type: String,
      	default: false
    }
});


module.exports = mongoose.model("products", productSchema);