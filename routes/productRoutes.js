const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// route for adding a product (admin only)
router.post("/addProduct", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true){
		res.send(`Authorization failed`);
	}
	else{
		productController.addProduct(req.body).then(result => {
			res.send(result);
		})
	}
});

// route for archiving a product (admin only)
router.put("/:productId/archive", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true){
		res.send(`Authorization failed`);
	}
	else{
		productController.archive(req).then(result => {
			res.send(result);
		})
	}
});

// route for re-activating a product (admin only)
router.post("/:productId/activate", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true){
		res.send(`Authorization failed`);
	}
	else{
		productController.activateProduct(req.params).then(result => {
			res.send(result);
		})
	}
});

// route for single product preview

router.get("/:productId/preview", (req,res)=>{
	productController.previewProduct(req.params).then(result => {
		res.send(result);
	});
});

// route for getting all active products

router.get("/all", (req,res) => {
	productController.allProducts().then(result => {
		res.send(result)
	});
});

// route for updating a product

router.put("/:productId/update", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true){
		res.send(`Authorization denied`);
	}
	else{
		productController.update(req.body,req.params).then(result => {
			res.send(result);
		});
	};
});


module.exports = router;
