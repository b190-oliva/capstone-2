const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// route for user registration

router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(result => res.send(result));
});

//route for user authentication

router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(result=>{
		res.send(result);
	});
});

// route for getting the details of a user

router.get("/details", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
		userController.getProfile({userId: userData.id}).then(result=>{
		res.send(result);
		});
});

// route to promote a user to an admin

router.put("/:userId/setAsAdmin", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true){
		res.send(`Access denied, please contact the administrator`);
	}
	else{
		userController.updateUser(req.params).then(result => {
			res.send(result)
		});
	};
});

// route to add orders on a specific user

router.post("/checkout", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true){
		let data = {
			productId: req.body.productId,
			userId: userData.id
		}
		userController.checkOut(data).then(result => res.send(result));
	}
	else{
		res.send(`failed`);
	}
});

// route to check all orders on a specific user

router.get("/allorders", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === true){
	userController.getAllOrders().then(result => res.send(result));
	}
	else{
		res.send(`Access denied`);
	}
});

// route for getting all orders of a specific user

router.get("/myOrders", auth.verify, (req,res)=>{
	let userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === false){
		userController.getMyOrders(userData).then(result => res.send(result));
	}
	else{
		res.send(false);
	}
});

// route for removing an order to cart

router.put("/clearCart", auth.verify, (req,res)=>{
	let userData = auth.decode(req.headers.authorization);
	userController.removeOrder(userData.id).then(result => res.send(result));
});

module.exports = router;